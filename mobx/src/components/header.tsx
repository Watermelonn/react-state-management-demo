import { observer } from "mobx-react";
import React, { FunctionComponent } from "react";
import { getFoodEmoji } from "../helpers";
import { initialiseStore } from "../stores/base.store";
import { Menu } from "./menu";

export const Title: FunctionComponent = observer(() => {
  const { foodStore, emojiStore } = initialiseStore();

  return (
    <h1>
      Dan's Super Sick Website (React Context Addition){" "}
      <p role="img" aria-label="food">
        Hey! You picked {foodStore.foodType}!{" "}
        {emojiStore?.emojiMode && getFoodEmoji(foodStore?.foodType)}
      </p>
    </h1>
  );
});

export const Header = () => (
  <header>
    <Title />
    <Menu />
  </header>
);
