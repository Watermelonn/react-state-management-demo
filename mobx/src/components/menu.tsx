import { observer } from "mobx-react";
import React, { FunctionComponent } from "react";
import { FoodTypeEnum } from "../enums";
import { getFoodEmoji } from "../helpers";
import { initialiseStore } from "../stores/base.store";

export interface IMenuItemProps {
  onClick: () => void;
  emoji: string;
}

const MenuItem: FunctionComponent<IMenuItemProps> = observer(
  ({ onClick, emoji, children }) => {
    const { emojiStore } = initialiseStore();

    return (
      <li className="menu-list-item" onClick={onClick}>
        <span className={"menu item"}>
          {children}{" "}
          {emojiStore?.emojiMode ? (
            <span role="img" aria-label="emoji">
              {emoji}
            </span>
          ) : null}
        </span>
      </li>
    );
  }
);

export const Menu: FunctionComponent = observer(() => {
  const { foodStore } = initialiseStore();

  const foodTypes = [
    FoodTypeEnum.PIZZA,
    FoodTypeEnum.BURGER,
    FoodTypeEnum.CHIPS,
    FoodTypeEnum.CAKE,
  ];

  return (
    <nav className="menu">
      <ul className="menu-list">
        {foodTypes.map((type) => (
          <MenuItem
            onClick={() => foodStore?.setFoodType(type)}
            emoji={getFoodEmoji(type)}
          >
            {type}
          </MenuItem>
        ))}
      </ul>
    </nav>
  );
});
