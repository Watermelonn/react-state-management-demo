import { action, makeObservable, observable } from "mobx";

export class EmojiStore {
  constructor() {
    makeObservable(this);
  }

  @observable
  public emojiMode: boolean = true;

  @action
  public setEmojiMode(emojiMode: boolean) {
    this.emojiMode = emojiMode;
  }
}
