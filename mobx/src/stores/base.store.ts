import { EmojiStore } from "./emoji.store";
import { FoodStore } from "./food.store";

export default class BaseStore {
  constructor() {
    this.emojiStore = new EmojiStore();
    this.foodStore = new FoodStore();
  }

  public emojiStore: EmojiStore;
  public foodStore: FoodStore;
}

let store: BaseStore | null = null;

export function initialiseStore(): BaseStore {
  if (store === null) {
    store = new BaseStore();
  }
  return store;
}
