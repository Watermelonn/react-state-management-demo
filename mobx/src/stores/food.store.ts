import { action, makeObservable, observable } from "mobx";
import { FoodTypeEnum } from "../enums";

export class FoodStore {
  constructor() {
    makeObservable(this);
  }

  @observable
  public foodType: FoodTypeEnum = FoodTypeEnum.PIZZA;

  @action
  public setFoodType(foodType: FoodTypeEnum) {
    this.foodType = foodType;
  }
}
