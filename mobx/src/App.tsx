import { observer } from "mobx-react";
import React, { FunctionComponent } from "react";
import "./App.css";
import { Content, Header } from "./components";
import { EmojiStore } from "./stores";

export interface IAppState {
  emojiMode: boolean;
}

export interface IAppProps {}

export const App: FunctionComponent<IAppProps> = observer(() => {
  const emojiStore = new EmojiStore();

  return (
    <div>
      <div className="emoji-toggle-container">
        <label className="emoji-toggle-label">
          <input
            type="checkbox"
            checked={emojiStore.emojiMode}
            onChange={(e) => emojiStore.setEmojiMode(e.target.checked)}
          />{" "}
          Enable Emoji Mode
        </label>
      </div>
      <Header />
      <Content />
    </div>
  );
});
