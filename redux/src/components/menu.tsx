import React, { FunctionComponent } from "react";
import { FoodTypeEnum } from "../enums";
import { getFoodEmoji } from "../helpers";
import { useAppDispatch, useAppSelector } from "../hooks";
import { FoodActions } from "../reducers/food.reducer";

export interface IMenuItemProps {
  onClick: () => void;
  emoji: string;
}

const MenuItem: FunctionComponent<IMenuItemProps> = ({
  onClick,
  emoji,
  children,
}) => {
  const { emojiMode } = useAppSelector((state) => state.emoji);

  return (
    <li className="menu-list-item" onClick={onClick}>
      <span className={"menu item"}>
        {children}{" "}
        {emojiMode ? (
          <span role="img" aria-label="emoji">
            {emoji}
          </span>
        ) : null}
      </span>
    </li>
  );
};

export const Menu: FunctionComponent = () => {
  const dispatch = useAppDispatch();

  const foodTypes = [
    FoodTypeEnum.PIZZA,
    FoodTypeEnum.BURGER,
    FoodTypeEnum.CHIPS,
    FoodTypeEnum.CAKE,
  ];

  return (
    <nav className="menu">
      <ul className="menu-list">
        {foodTypes.map((type) => (
          <MenuItem
            onClick={() =>
              dispatch({
                type: FoodActions.SET_FOOD_TYPE,
                payload: { foodType: type },
              })
            }
            emoji={getFoodEmoji(type)}
          >
            {type}
          </MenuItem>
        ))}
      </ul>
    </nav>
  );
};
