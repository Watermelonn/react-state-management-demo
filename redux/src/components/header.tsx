import React, { FunctionComponent } from "react";
import { getFoodEmoji } from "../helpers";
import { useAppSelector } from "../hooks";
import { Menu } from "./menu";

export const Title: FunctionComponent = () => {
  const { food, emoji } = useAppSelector((state) => state);

  return (
    <h1>
      Dan's Super Sick Website (React Context Addition){" "}
      <p role="img" aria-label="food">
        Hey! You picked {food.foodType}!{" "}
        {emoji?.emojiMode && getFoodEmoji(food?.foodType)}
      </p>
    </h1>
  );
};

export const Header = () => (
  <header>
    <Title />
    <Menu />
  </header>
);
