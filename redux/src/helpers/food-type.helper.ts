import { FoodTypeEnum } from "../enums";

export const getFoodEmoji = (foodType: FoodTypeEnum) => {
  switch (foodType) {
    case FoodTypeEnum.PIZZA:
      return "🍕";
    case FoodTypeEnum.BURGER:
      return "🍔";
    case FoodTypeEnum.CHIPS:
      return "🍟";
    case FoodTypeEnum.CAKE:
      return "🍰";
  }
};
