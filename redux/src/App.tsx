import React, { FunctionComponent } from "react";
import "./App.css";
import { Content, Header } from "./components";
import { useAppDispatch, useAppSelector } from "./hooks";
import { EmojiActions } from "./reducers/emoji.reducer";

export interface IAppState {
  emojiMode: boolean;
}

export interface IAppProps {}

export const App: FunctionComponent<IAppProps> = () => {
  const { emojiMode } = useAppSelector((state) => state.emoji);
  const dispatch = useAppDispatch();

  return (
    <div>
      <div className="emoji-toggle-container">
        <label className="emoji-toggle-label">
          <input
            type="checkbox"
            checked={emojiMode}
            onChange={(e) =>
              dispatch({
                type: EmojiActions.SET_EMOJI_MODE,
                payload: { emojiMode: e.target.checked },
              })
            }
          />{" "}
          Enable Emoji Mode
        </label>
      </div>
      <Header />
      <Content />
    </div>
  );
};
