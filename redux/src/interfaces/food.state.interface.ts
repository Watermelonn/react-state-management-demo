import { FoodTypeEnum } from "../enums";

export interface IFoodState {
  foodType: FoodTypeEnum;
}
