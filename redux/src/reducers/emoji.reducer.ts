import { createReducer } from "@reduxjs/toolkit";
import { IEmojiState } from "../interfaces";

export enum EmojiActions {
  SET_EMOJI_MODE = "SET_EMOJI_MODE",
}

const initState: IEmojiState = {
  emojiMode: true,
};

const emojiReducer = createReducer(initState, {
  SET_EMOJI_MODE: (state: IEmojiState, action) => {
    state.emojiMode = action.payload.emojiMode;
  },
});

export default emojiReducer;
