import { createReducer } from "@reduxjs/toolkit";
import { FoodTypeEnum } from "../enums";
import { IFoodState } from "./../interfaces";

export enum FoodActions {
  SET_FOOD_TYPE = "SET_FOOD_TYPE",
}

const initState: IFoodState = {
  foodType: FoodTypeEnum.PIZZA,
};

const foodReducer = createReducer(initState, {
  SET_FOOD_TYPE: (state: IFoodState, action) => {
    state.foodType = action.payload.foodType!;
  },
});

export default foodReducer;
