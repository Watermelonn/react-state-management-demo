import { configureStore } from "@reduxjs/toolkit";
import { emojiReducer, foodReducer } from "./reducers";

export const store = configureStore({
  reducer: {
    emoji: emojiReducer,
    food: foodReducer,
  },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
