export enum FoodTypeEnum {
  PIZZA = "Pizza",
  CHIPS = "Chips",
  BURGER = "Burger",
  CAKE = "Cake",
}
