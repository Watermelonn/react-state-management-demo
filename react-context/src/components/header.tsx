import React, { FunctionComponent } from "react";
import { EmojiModeContext, FoodContext } from "../context";
import { getFoodEmoji } from "../helpers";
import { Menu } from "./menu";

const Title: FunctionComponent = () => (
  <EmojiModeContext.Consumer>
    {(emojiMode) => (
      <FoodContext.Consumer>
        {({ foodType }) => (
          <h1>
            Dan's Super Sick Website (React Context Addition){" "}
            <p role="img" aria-label="food">
              Hey! You picked {foodType}! {emojiMode && getFoodEmoji(foodType)}
            </p>
          </h1>
        )}
      </FoodContext.Consumer>
    )}
  </EmojiModeContext.Consumer>
);

export const Header = () => (
  <header>
    <Title />
    <Menu />
  </header>
);
