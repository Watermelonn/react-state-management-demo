import React, { FunctionComponent, useContext } from "react";
import { EmojiModeContext, FoodContext } from "../context";
import { FoodTypeEnum } from "../enums";
import { getFoodEmoji } from "../helpers";

export interface IMenuItemProps {
  onClick: () => void;
  emoji: string;
}

const MenuItem: FunctionComponent<IMenuItemProps> = ({
  onClick,
  emoji,
  children,
}) => (
  <EmojiModeContext.Consumer>
    {(emojiMode: boolean) => (
      <li className="menu-list-item" onClick={onClick}>
        <span className={"menu item"}>
          {children}{" "}
          {emojiMode ? (
            <span role="img" aria-label="emoji">
              {emoji}
            </span>
          ) : null}
        </span>
      </li>
    )}
  </EmojiModeContext.Consumer>
);

export const Menu: FunctionComponent = () => {
  const { setFoodType } = useContext(FoodContext);

  const foodTypes = [
    FoodTypeEnum.PIZZA,
    FoodTypeEnum.BURGER,
    FoodTypeEnum.CHIPS,
    FoodTypeEnum.CAKE,
  ];

  return (
    <nav className="menu">
      <ul className="menu-list">
        {foodTypes.map((type) => (
          <MenuItem
            onClick={() => setFoodType(type)}
            emoji={getFoodEmoji(type)}
          >
            {type}
          </MenuItem>
        ))}
      </ul>
    </nav>
  );
};
