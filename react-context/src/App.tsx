import React, { FunctionComponent, useState } from "react";
import "./App.css";
import { Content, Header } from "./components";
import { EmojiModeContext, FoodContext } from "./context";
import { FoodTypeEnum } from "./enums";

export interface IAppState {
  emojiMode: boolean;
}

export interface IAppProps {}

export const App: FunctionComponent<IAppProps> = () => {
  const [emojiMode, setEmojiMode] = useState(true);
  const [foodType, setFoodType] = useState(FoodTypeEnum.PIZZA);

  return (
    <div>
      <div className="emoji-toggle-container">
        <label className="emoji-toggle-label">
          <input
            type="checkbox"
            checked={emojiMode}
            onChange={(e) => setEmojiMode(e.target.checked)}
          />{" "}
          Enable Emoji Mode
        </label>
      </div>
      <FoodContext.Provider value={{ foodType, setFoodType }}>
        <EmojiModeContext.Provider value={emojiMode}>
          <Header />
          <Content />
        </EmojiModeContext.Provider>
      </FoodContext.Provider>
    </div>
  );
};
