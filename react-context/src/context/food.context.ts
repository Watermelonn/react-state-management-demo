import { FoodTypeEnum } from "../enums";
import { createContext } from "react";

export const FoodContext = createContext({
  foodType: FoodTypeEnum.PIZZA,
  setFoodType: (foodType: FoodTypeEnum) => {},
});
