import { createContext } from "react";

export const EmojiModeContext = createContext(true);
